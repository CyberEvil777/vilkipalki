from django.urls import path
from market_service.api.terminal_groups.views import TerminalGroupView


urlpatterns = [
    path("terminal-group/get-terminal-group", TerminalGroupView.as_view())
]

from rest_framework.response import Response
from rest_framework import status
from market_service.api.terminal_groups.serializers import TerminalGroupSerializerGet
from django.conf import settings
from rest_framework import mixins
from rest_framework import generics


class TerminalGroupView(mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        generics.GenericAPIView):

    serializer_class = TerminalGroupSerializerGet

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        result = None
        if serializer.is_valid():
            terminal_groups = settings.API_INSTANCE_IIKO.terminal_groups(
                organization_ids=serializer.validated_data['organizationIds'],
                include_disabled=serializer.validated_data['includeDisabled']
            )
            result = Response(terminal_groups, status=status.HTTP_201_CREATED)

        return result


from rest_framework import serializers


class TerminalGroupSerializerGet(serializers.Serializer):
    """Сераилазитор для получения терминальной группы."""

    organizationIds = serializers.ListField(child=serializers.CharField())
    includeDisabled = serializers.BooleanField()
    returnExternalData = serializers.ListField(child=serializers.CharField())
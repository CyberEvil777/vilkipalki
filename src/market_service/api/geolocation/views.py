from market_service.api.geolocation.serializers import AddressSerializer
from market_service.apps.organizations.models import Address
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from market_service.core.custom_iiko import IikoTransportCustom
from market_service.apps.organizations.models import Address
from django.conf import settings


class AddressViewSet(ModelViewSet):
    """View для адресов."""

    queryset = Address.objects.all()
    serializer_class = AddressSerializer


class OrganizationIIKOView(APIView):
    """
    Ручка используется для подтягивания организаций с iiko.
    Он вызывается при добавление новой точки
    """
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        organizations = settings.API_INSTANCE_IIKO.organizations()
        organizations_ids_iiko = [organization['id'] for organization in organizations['organizations']]
        organizations_data = [organization for organization in organizations['organizations']]

        # получаем айди организаций iiko которые уже существуют
        organizations_ids_exists = Address.objects.filter(
            iiko_id__in=organizations_ids_iiko
        ).values_list('iiko_id', flat=True)

        new_address = []

        for organization in organizations_data:
            if organization['id'] not in organizations_ids_exists:
                new_address.append(
                    Address(
                    name=organization['name'],
                    iiko_id=organization['id'])
                )

        Address.objects.bulk_create(new_address)

        return Response(data={'msg': 'Данные загружены'})

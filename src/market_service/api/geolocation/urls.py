from django.urls import path
from market_service.api.geolocation.views import AddressViewSet, OrganizationIIKOView


urlpatterns = [
    path("address/", AddressViewSet.as_view({'get': 'list'}), name="address"),
    path("address-iiko/", OrganizationIIKOView.as_view(), name="adress-iiko")
]

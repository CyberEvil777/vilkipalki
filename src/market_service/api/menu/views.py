from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from market_service.api.menu.serializers import MenuSerializer
from market_service.apps.menu.models import Category
from market_service.api.menu.serializers import CategorySerializer, FoodIIKOSerializer
from market_service.apps.menu.models import Food


class MenuViewSet(ModelViewSet):
    queryset = Category.objects.all().prefetch_related("food")
    serializer_class = MenuSerializer

    def create(self, request, *args, **kwargs):
        serializer = CategorySerializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        created_data = []

        for category in serializer.data:
            category_dict = dict(category)

            category = Category.objects.filter(
                iiko_id=category_dict['iiko_id']
            ).first()

            if not category:
                self.perform_create(serializer)
                created_data.append(
                    CategorySerializer(category)
                )

        headers = self.get_success_headers(serializer.data)
        return Response(
            {
                "message": "Iiko записи были успешно созданы"
            },
            status=status.HTTP_201_CREATED,
            headers=headers
        )


class ProductViewSet(ModelViewSet):
    queryset = Food.objects.all()
    serializer_class = FoodIIKOSerializer

    def create(self, request, *args, **kwargs):
        serializer = FoodIIKOSerializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        created_data = []

        for product in serializer.data:
            product_dict = dict(product)

            food = Food.objects.filter(
                iiko_id=product_dict['iiko_id']
            ).first()

            if not food:
                self.perform_create(serializer)
                created_data.append(
                    FoodIIKOSerializer(food)
                )

        headers = self.get_success_headers(serializer.data)
        return Response(
            {
                "message": "Iiko записи были успешно созданы"
            },
            status=status.HTTP_201_CREATED,
            headers=headers
        )

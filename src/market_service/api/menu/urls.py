from django.urls import path

from market_service.api.menu.views import MenuViewSet, ProductViewSet


urlpatterns = [
    path("menu/", MenuViewSet.as_view(
        {
            'get': 'list',
            'post': 'create'
        }
    ), name="menu"),
    path("menu/iiko-products", ProductViewSet.as_view(
        {
            'post': 'create'
        }
    ))
]

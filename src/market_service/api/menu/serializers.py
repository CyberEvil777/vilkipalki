from rest_framework import serializers

from market_service.apps.menu.models import Category, Food


# from .models import Category, Food

class FoodSerializer(serializers.ModelSerializer):

    image = serializers.SerializerMethodField("convert_photo_base64")

    def convert_photo_base64(self, obj):
        return obj.get_convert_base64(obj)

    class Meta:
        model = Food
        fields = ['id', 'name', 'iiko_id', 'description', 'price', 'category', 'image']


class FoodIIKOSerializer(serializers.ModelSerializer):

    class Meta:
        model = Food
        fields = ['iiko_id', 'category_iiko_id', 'name']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name', 'iiko_id']


class MenuSerializer(serializers.ModelSerializer):
    food = FoodSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['id', 'name', 'food']

from rest_framework import serializers
from market_service.apps.orders.models import OrderItem


class PaymentItemSerializer(serializers.Serializer):
    paymentTypeKind = serializers.CharField()
    sum = serializers.IntegerField()
    paymentTypeId = serializers.UUIDField()
    isProcessedExternally = serializers.BooleanField()
    isFiscalizedExternally = serializers.BooleanField()


class OrderItemSerializer(serializers.Serializer):
    productId = serializers.UUIDField()
    type = serializers.CharField()
    amount = serializers.IntegerField()


class CustomerIIKOSerializer(serializers.Serializer):
    name = serializers.CharField()
    surname = serializers.CharField()
    comment = serializers.CharField()
    email = serializers.CharField()
    gender = serializers.CharField()


class OrderIIKOSerializer(serializers.Serializer):
    sourceKey = serializers.CharField()
    phone = serializers.CharField()
    completeBefore = serializers.DateTimeField()
    items = OrderItemSerializer(many=True)
    payments = PaymentItemSerializer(many=True)
    customer = CustomerIIKOSerializer()


class OrderCreateIIKOSerializer(serializers.Serializer):
    order = OrderIIKOSerializer()


class OrderItemResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'

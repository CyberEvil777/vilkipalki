from django.urls import path

from market_service.api.orders.views import OrderViewSet


urlpatterns = [
    path("menu/order", OrderViewSet.as_view(
        {
            'post': 'create'
        }
    )),
    path("menu/order-list", OrderViewSet.as_view(
        {
            'get': 'list'
        }
    )),
]

from rest_framework.viewsets import ModelViewSet
from market_service.api.orders.serializers import OrderCreateIIKOSerializer
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from market_service.apps.customer.models import Customer
from market_service.apps.orders.utils import create_order_items
from market_service.apps.orders.models import OrderItem
from market_service.api.orders.serializers import OrderItemResultSerializer
from rest_framework.response import Response


class OrderViewSet(ModelViewSet):
    serializer_class = OrderCreateIIKOSerializer

    def list(self, request, *args, **kwargs):
        queryset = OrderItem.objects.all()
        serializer = OrderItemResultSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        order_data = request.data['order']
        order_data["orderTypeId"] = settings.ORDER_TYPE_ID
        customer_data = Customer.create_customer(request)

        create_order_items(request_data=request.data, customer_instance=customer_data)
        # order_data = data_from_request.data['order']
        # customer_data = order_data['customer']
        # customer_data['phone'] = order_data['phone']

        order_response = settings.API_INSTANCE_IIKO.order_create(
            organization_id=settings.ORGANIZATION_ID,
            terminal_group_id=settings.TERMINAL_GROUP_ID,
            order=order_data
        )

        return Response(
            order_response,
            status=status.HTTP_201_CREATED,
        )


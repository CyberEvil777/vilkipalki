from django.contrib import admin

from market_service.apps.customer.models import Customer


@admin.register(Customer)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'email', 'gender', 'phone')
    list_filter = ('gender',)
    search_fields = ('name', 'surname', 'email', 'phone')

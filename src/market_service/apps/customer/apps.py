from django.apps import AppConfig


class CustomerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'market_service.apps.customer'
    verbose_name = "Клиенты"

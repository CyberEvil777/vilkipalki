from django.db import models


class Customer(models.Model):

    gender_choices = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('Spec', 'NotSpecified')
    )

    name = models.CharField(verbose_name="Имя", max_length=255)
    surname = models.CharField(verbose_name="Фамилия", max_length=255)
    comment = models.TextField(verbose_name="Комментарий к заказу")
    email = models.EmailField(verbose_name="E-mail")
    gender = models.CharField(verbose_name="Пол", choices=gender_choices, max_length=15)
    phone = models.CharField(max_length=50)

    @staticmethod
    def create_customer(data_from_request):
        """Создает клиента после отпарвки запроса."""
        order_data = data_from_request.data['order']
        customer_data = order_data['customer']
        customer_data['phone'] = order_data['phone']

        customer_instance = Customer(
            name=customer_data['name'],
            gender=customer_data['gender'],
            comment=customer_data['comment'],
            phone=customer_data['phone'],
            surname=customer_data['surname']
        )
        customer_instance.save()
        return customer_instance

    def __str__(self):
        return f"{self.name} {self.surname}"

    class Meta:
        verbose_name = "Клиенты"
        verbose_name_plural = verbose_name

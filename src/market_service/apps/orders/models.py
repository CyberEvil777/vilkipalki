from market_service.apps.customer.models import Customer
from django.db import models


class OrderInfo(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, verbose_name="Клиент")
    payment_type = models.CharField(max_length=200, verbose_name="Тип оплаты")

    def __str__(self):
        return f"Клиент: {self.customer.pk} Тип оплаты: {self.payment_type}"

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"


class OrderItem(models.Model):
    """Элемент заказа"""
    amount = models.IntegerField(verbose_name="Количество единиц продукта", null=True)
    product_iiko_id = models.CharField(max_length=100,
                                       verbose_name='id товара в Iiko', null=True,
                                       blank=True)
    order_info_item = models.ForeignKey(OrderInfo, on_delete=models.CASCADE)

    def __str__(self):
        return f'Количество: {self.amount} Product Iiiko Id: {self.product_iiko_id}'

    class Meta:
        verbose_name = "Элемент заказа"
        verbose_name_plural = "Элементы заказа"
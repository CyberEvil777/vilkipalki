from market_service.apps.orders.models import OrderInfo, OrderItem


def create_order_items(request_data, customer_instance):
    types_payments = request_data['order']['payments']
    list_str_types = []

    for type_pay in types_payments:
        list_str_types.append(type_pay['paymentTypeKind'])

    string_types = "".join(list_str_types)
    order_info = OrderInfo(payment_type=string_types,
                           customer=customer_instance)
    order_info.save()

    items_orders = request_data['order']['items']
    orders = [OrderItem(amount=order['amount'], product_iiko_id=order['productId'], order_info_item=order_info) for order in items_orders]

    OrderItem.objects.bulk_create(orders)

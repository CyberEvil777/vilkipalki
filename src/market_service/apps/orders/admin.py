from market_service.apps.orders.models import OrderItem, OrderInfo
from django.contrib import admin


class OrderItemTabularInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    readonly_fields = ('amount', 'product_iiko_id', 'order_info_item', )


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    readonly_fields = ('amount', 'product_iiko_id', 'order_info_item')


@admin.register(OrderInfo)
class OrderInfoAdmin(admin.ModelAdmin):
    readonly_fields = ('customer', 'payment_type')
    inlines = [OrderItemTabularInline]




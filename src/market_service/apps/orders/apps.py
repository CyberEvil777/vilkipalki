from django.apps import AppConfig


class MenuConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = "market_service.apps.orders"
    verbose_name = "Заказы"

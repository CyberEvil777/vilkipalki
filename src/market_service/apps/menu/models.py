import base64

from django.db import models


def image_as_base64(image_file, format='png'):
    """
    :param `image_file` for the complete path of image.
    :param `format` is format for image, eg: `png` or `jpg`.
    """
    # if not os.path.isfile(image_file):
    #     return None

    encoded_string = ''
    with open(image_file, 'rb') as img_f:
        encoded_string = base64.b64encode(img_f.read())
    return f'data:image/{format};base64,{encoded_string.decode("utf-8")}'


class Category(models.Model):
    name = models.CharField(verbose_name="Название", max_length=255)
    description = models.TextField(verbose_name="Описание",)
    iiko_id = models.CharField(max_length=100,
                               verbose_name='id категории в Iiko', null=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name


class Food(models.Model):
    name = models.CharField(verbose_name="Название", max_length=255)
    description = models.TextField(verbose_name="Описание", null=True)
    price = models.SmallIntegerField(verbose_name="Цена", null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="food", null=True)
    image = models.ImageField(upload_to='food_images/')
    iiko_id = models.CharField(max_length=100,
                               verbose_name='id товара в Iiko', null=True, help_text="productId")
    category_iiko_id = models.CharField(max_length=100,
                               verbose_name='id категории товара в Iiko', null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Еда"
        verbose_name_plural = verbose_name

    def get_convert_base64(self, obj):
        result_image = None
        if hasattr(obj.image, 'path'):
            result_image = image_as_base64(obj.image.path)
        return result_image


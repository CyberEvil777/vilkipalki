from django.apps import AppConfig


class MenuConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = "market_service.apps.menu"
    verbose_name = "Меню"

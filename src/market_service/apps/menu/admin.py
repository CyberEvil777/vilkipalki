from django.contrib import admin

from market_service.apps.menu.models import Category, Food


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('iiko_id', )


@admin.register(Food)
class FoodAdmin(admin.ModelAdmin):
    readonly_fields = ('iiko_id', )


# Generated by Django 4.2.7 on 2023-11-22 15:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0002_alter_food_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='iiko_id',
            field=models.CharField(max_length=100, null=True, verbose_name='id категории в Iiko'),
        ),
    ]

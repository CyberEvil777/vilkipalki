# Generated by Django 4.2.7 on 2023-12-08 19:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0002_address_iiko_id_address_name_alter_address_address'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='address',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Адрес'),
        ),
        migrations.AlterField(
            model_name='address',
            name='iiko_id',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='id категории в Iiko'),
        ),
        migrations.AlterField(
            model_name='address',
            name='name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Имя организации'),
        ),
    ]

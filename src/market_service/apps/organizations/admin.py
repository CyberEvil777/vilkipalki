from django.contrib import admin
from django.contrib.auth.models import Group, User

from market_service.apps.organizations.models import Address


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    readonly_fields = ('iiko_id', )


admin.site.unregister(User)
admin.site.unregister(Group)


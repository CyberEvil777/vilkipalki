from django.apps import AppConfig


class CityConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "market_service.apps.organizations"
    verbose_name = "Города"

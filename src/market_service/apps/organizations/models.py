# from django.contrib.gis.db import models
from django.db import models


class Address(models.Model):
    """Модель для точек китайка"""

    address = models.CharField(verbose_name="Адрес", max_length=255,
                               null=True, blank=True)

    iiko_id = models.CharField(max_length=100,
                               verbose_name='id категории в Iiko', null=True,
                               blank=True)
    name = models.CharField(
        max_length=255,
        verbose_name="Имя организации",
        null=True, blank=True
    )

    def __str__(self):
        data_info = ''

        if self.name is not None:
            data_info = f'Имя {self.name}'

        if self.iiko_id is not None:
            data_info = f'Iiko_id {self.iiko_id}'

        if self.address is not None:
            data_info = f'Адрес {self.address}'

        return f'{data_info}'

    # lоcation = models.PointField(
    #     verbose_name="Геопозиция",
    #     geography=True,
    #     help_text="Первым значением идет долгота longitude, а вторым значением широта latitude",
    # )

    class Meta:
        verbose_name = "Ресторан Kitaika"
        verbose_name_plural = verbose_name


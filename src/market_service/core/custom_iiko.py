"""
Кастомный модуль Iiko для обработки данных.
Он изменяет код библиотеки, потому что библиотека содержит некоторые
не валидные описания данных в pydantic, поэтому пришлост изменять библиотеку под наши
нужды.
"""

from pyiikocloudapi import IikoTransport
from pyiikocloudapi.api import BaseAPI
import requests
from pyiikocloudapi.exception import TokenException, PostException
from pyiikocloudapi.models import *


class IikoTransportCustom(IikoTransport):
    def menu_by_id(self, external_menu_id: str, organization_ids: List[str], price_category_id: str = None,
                   timeout=BaseAPI.DEFAULT_TIMEOUT) -> Union[CustomErrorModel, BaseMenuByIdModel]:

        data = {
            "externalMenuId": external_menu_id,
            "organizationIds": organization_ids,
        }

        if price_category_id is not None:
            data["priceCategoryId"] = price_category_id

        try:
            response = self._post_request(
                url="/api/2/menu/by_id",
                data=data,
                model_response_data=None,
                timeout=timeout
            )
            return response
        except requests.exceptions.RequestException as err:
            raise TokenException(self.__class__.__qualname__,
                                 self.nomenclature.__name__,
                                 f"Не удалось получить внешнее меню по ID.: \n{err}")
        except TypeError as err:
            raise TypeError(self.__class__.__qualname__,
                            self.nomenclature.__name__,
                            f"Не удалось получить внешнее меню по ID.: \n{err}")

    def organizations(self, organization_ids: List[str] = None, return_additional_info: bool = None,
                      include_disabled: bool = None, timeout=BaseAPI.DEFAULT_TIMEOUT) -> Union[
        CustomErrorModel, BaseOrganizationsModel]:
        """
        Возвращает организации, доступные пользователю API-login.
        :param organization_ids: Organizations IDs which have to be returned. By default - all organizations from apiLogin.
        :param return_additional_info: A sign whether additional information about the organization should be returned (RMS version, country, restaurantAddress, etc.), or only minimal information should be returned (id and name).
        :param include_disabled: Attribute that shows that response contains disabled organizations.
        :return:
        """
        #         https://api-ru.iiko.services/api/1/organizations
        data = {}
        if organization_ids is not None:
            data["organizationIds"] = organization_ids
        if return_additional_info is not None:
            data["returnAdditionalInfo"] = return_additional_info
        if include_disabled is not None:
            data["includeDisabled"] = include_disabled
        try:

            response_data = self._post_request(
                url="/api/1/organizations",
                data=data,
                model_response_data=None,
                timeout=timeout
            )
            if isinstance(response_data, BaseOrganizationsModel):
                self.__convert_org_data(data=response_data)
            return response_data

        except requests.exceptions.RequestException as err:
            raise TokenException(self.__class__.__qualname__,
                                 self.organizations.__name__,
                                 f"Не удалось получить организации: \n{err}")
        except TypeError as err:
            raise TypeError(self.__class__.__qualname__,
                            self.organizations.__name__,
                            f"Не удалось получить организации: \n{err}")

    def order_create(self, organization_id: str, terminal_group_id: str, order: dict,
                     create_order_settings: Optional[int] = None, timeout=BaseAPI.DEFAULT_TIMEOUT) -> Union[
        CustomErrorModel,
        BaseCreatedOrderInfoModel]:
        """"""

        data = {
            "organizationId": organization_id,
            "terminalGroupId": terminal_group_id,
            "order": order,
        }
        if create_order_settings is not None:
            data["createOrderSettings"] = create_order_settings
        response = None
        try:
            response = self._post_request(
                url="/api/1/deliveries/create",
                data=data,
                model_response_data=None,
                timeout=timeout
            )
            return response
        except requests.exceptions.RequestException as err:
            raise PostException(self.__class__.__qualname__,
                                self.order_create.__name__,
                                f"Не удалось создать заказ из за: \n{err}")
        except TypeError as err:
            raise TypeError(self.__class__.__qualname__,
                            self.order_create.__name__,
                            f"Не удалось создать заказ из за: \n{err}")


# если запускается для тестирования работы клиента
if __name__ == '__main__':
    api = IikoTransportCustom(api_login='1c43279d-e79', base_url='https://api-ru.iiko.services')
    organizations = api.organizations()
    data_about_terminal_groups = api.terminal_groups(organization_ids=['ad9d505b-79c8-4ee0-aaeb-782b4330a16d'],
                                                     include_disabled=False)
    data = api.menu_by_id(external_menu_id='17917',
                          organization_ids=['ad9d505b-79c8-4ee0-aaeb-782b4330a16d'])
    print(data['itemCategories'][0]['items'])
    a = 4
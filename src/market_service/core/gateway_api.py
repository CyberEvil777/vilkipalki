from pyiikocloudapi import IikoTransport
import requests
from market_service.settings.base import BASE_URL_IIKO, LOGIN_ACCESS_IIKO, BASE_SITE_URL


class IikoIntegration:

    def __init__(self):
        self.api = IikoTransport(api_login=LOGIN_ACCESS_IIKO, base_url=BASE_URL_IIKO)
        self.url_api_for_upload = BASE_SITE_URL

    def nomenclature_upload(self, organization_id):
        """Возвращает True если загрузка прошла успешно"""
        data = self.api.nomenclature(organization_id)
        list_with_upload = []

        for category in data.product_categories:
            list_with_upload.append(
                {
                    'iiko_id': category.id,
                    'name': category.name
                }
            )

        response = requests.post(url=f'{self.url_api_for_upload}/api/menu/',
                                 json=list_with_upload)

        is_success = True if response.status_code == 200 else False
        return is_success

    def product_items_upload(self, organization_ids: list):
        externals_menu = self.api.menu().external_menus
        data_with_products = []
        data = None
        for menu in externals_menu:
            data = self.api.menu_by_id(external_menu_id=menu.id,
                                       organization_ids=organization_ids)

        items = data['itemCategories']

        for data in items:
            for product in data['items']:
                data_with_products.append(
                    {
                        'name': product['name'],
                        'iiko_id': product['itemId'],
                        'category_iiko_id': product['productCategoryId']
                    }
                )

        response = requests.post(url=f'{self.url_api_for_upload}/api/menu/iiko-products',
                                 json=data_with_products)
        is_success = True if response.status_code == 201 else False
        return is_success

import asyncio
from datetime import datetime, timedelta

from arq import create_pool
from arq.connections import RedisSettings
from market_service.core.gateway_api import IikoIntegration
from market_service.settings.base import API_INSTANCE_IIKO
import logging

logging.basicConfig(format='%(process)d-%(levelname)s-%(message)s')


async def upload_category_organizations(ctx):
    logging.info('Задача на выгрузку запустилась: ', datetime.now())
    organizations = API_INSTANCE_IIKO.organizations()
    for organization in organizations['organizations']:
        try:
            logging.info(
                f'Задача на выгрузку запустилась для организации: {organization["id"]} {datetime.now()}',
            )
            is_upload = IikoIntegration().nomenclature_upload(organization['id'])
            if is_upload:
                logging.info(f"Данные о категориях в организации {organization['id']} прогрузились")
        except KeyError:
            continue


async def upload_product_items(ctx):
    logging.info('Задача на выгрузку продуктов запустилась: ', datetime.now())
    organizations = API_INSTANCE_IIKO.organizations()
    organizations_ids = [organization['id'] for organization in organizations['organizations']]
    IikoIntegration().product_items_upload(organizations_ids)


async def main():
    redis = await create_pool(RedisSettings())
    await redis.enqueue_job('upload_category_organizations', _defer_by=timedelta(seconds=10))
    await redis.enqueue_job('upload_product_items', _defer_by=timedelta(seconds=10))


class WorkerSettings:
    functions = [upload_category_organizations, upload_product_items]


if __name__ == '__main__':
    asyncio.run(main())
